package com.assesment.task.model

import com.google.gson.annotations.SerializedName
import com.assesment.task.base.BaseModel

/**
 * Data Model class for Currency Rate list
 * Author: Muhammad Mudasir
 */
data class CurrencyRates(@SerializedName("base") var base: String,
                         @SerializedName("date") var date: String,
                         @SerializedName("rates") var rates: Map<String, String>) : BaseModel()

