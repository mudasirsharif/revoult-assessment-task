package com.assesment.task

import android.app.Application
import android.content.Context
import com.assesment.task.injection.component.ApplicationComponent
import com.assesment.task.injection.component.DaggerApplicationComponent
import com.assesment.task.injection.module.OperationalDataManagerModule
import com.assesment.task.injection.module.RetrofitClientModule

/**
 * The application class in which all the app-wide initializations happens
 * Author: Muhammad Mudasir
 */
class RevoltTaskApplication : Application() {

    init {
        instance = this
    }

    /**
     * Companion object to provide the static methods and variables for Application context and Application Component
     */
    companion object {

        private lateinit var applicationComponent: ApplicationComponent
        fun getAppComponent(): ApplicationComponent {
            return applicationComponent
        }

        private lateinit var instance: RevoltTaskApplication
        fun applicationContext(): Context {
            return instance.applicationContext
        }
    }

    /**
     * Building the dagger component here
     */
    override fun onCreate() {
        super.onCreate()

        applicationComponent = DaggerApplicationComponent.builder()
                .retrofitClientModule(RetrofitClientModule())
                .operationalDataManagerModule(OperationalDataManagerModule())
                .build()
    }

}