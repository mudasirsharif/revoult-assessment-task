package com.assesment.task.viewModel

import androidx.lifecycle.MutableLiveData
import com.assesment.task.base.BaseViewModel
import com.assesment.task.view.listeners.OnAmountChangeListener

/**
 * ViewModel Class for Currency Item in Recycler View
 * Author: Muhammad Mudasir
 */
class CurrencyItemViewModel : BaseViewModel() {

    var position: Int = 0
    var currencyTitle: MutableLiveData<String> = MutableLiveData()
    var currencyName: MutableLiveData<String> = MutableLiveData()
    var currencyRate: MutableLiveData<String> = MutableLiveData()
    var flag: MutableLiveData<Int> = MutableLiveData()
    var listener: OnAmountChangeListener? = null

    /**
     * OnTextChange listener to update the currency values on base currency value change
     */
    fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {
        listener?.onAmountChange(s.toString())
    }

    /**
     * Method to check if the item is a base currency
     * @return - true if item is a base currency
     */
    fun isBaseCurrency(): Boolean = position == 0
}