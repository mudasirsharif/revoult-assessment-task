package com.assesment.task.viewModel

import androidx.lifecycle.ViewModel
import com.assesment.task.base.BaseModel

/**
 * SharedViewModel Class which can be shared between fragments
 * Author: Muhammad Mudasir
 */
class SharedViewModel : ViewModel() {

    var model: BaseModel = BaseModel()
}