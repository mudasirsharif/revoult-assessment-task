package com.assesment.task.viewModel

import androidx.databinding.ObservableArrayList
import androidx.databinding.ObservableList
import androidx.lifecycle.MutableLiveData
import com.assesment.task.BR
import com.assesment.task.R
import com.assesment.task.RevoltTaskApplication
import com.assesment.task.base.BaseModel
import com.assesment.task.base.BaseViewModel
import com.assesment.task.dataManager.OperationalDataManager
import com.assesment.task.model.CurrencyRates
import com.assesment.task.utils.Constants
import com.assesment.task.utils.MiscUtils
import com.assesment.task.view.listeners.OnAmountChangeListener
import com.assesment.task.view.listeners.OnItemClickListener
import com.mynameismidori.currencypicker.ExtendedCurrency
import io.reactivex.disposables.CompositeDisposable
import me.tatarka.bindingcollectionadapter2.ItemBinding
import java.util.*
import javax.inject.Inject

/**
 * ViewModel Class for Currency List Fragment
 * Author: Muhammad Mudasir
 */
class CurrencyListFragmentViewModel : BaseViewModel() {

    val currencyItemViewModelList: ObservableList<CurrencyItemViewModel> = ObservableArrayList<CurrencyItemViewModel>()
    val currencyRatesList: ArrayList<Float> = arrayListOf()

    val userClickResponse: MutableLiveData<Int> = MutableLiveData()

    private var baseCurrencyCode = Constants.VALUE_BASE_CURRENCY_KEY
    private var baseCurrencyValue = 100f

    private var isInitialFetch = true

    val compositeDisposable = CompositeDisposable()

    /**
     * Recycler View binding using Binding Collection adapter library
     */
    var singleItem: ItemBinding<CurrencyItemViewModel>? = ItemBinding.of<CurrencyItemViewModel>(BR.itemViewModel, R.layout.currency_item_layout)
            .bindExtra(BR.listener, object : OnItemClickListener {
                override fun onItemClick(item: Any) {
                    val position = (item as CurrencyItemViewModel).position
                    /** Check to skip the base currency from swapping **/
                    if (position != 0)
                        userClickResponse.value = position
                }
            })

    @Inject
    lateinit var operationalDataManager: OperationalDataManager

    /**
     * Initialization block to inject the OperationalDataManager object
     */
    init {

        RevoltTaskApplication.getAppComponent().inject(this)
    }

    /**
     * Method to initiate the get currencyList request
     */
    fun getCurrencyList() {

        val observable = operationalDataManager.currencyRateServiceRequest(getCurrencyParams(baseCurrencyCode))

        observable.subscribe(this)

        disposable?.let { compositeDisposable.add(it) }
    }

    /**
     * Override onNext of the observer to do the service data operation
     */
    override fun onNext(t: BaseModel) {
        super.onNext(t)

        if (isInitialFetch) {
            serviceResponse.value = t
            isInitialFetch = false
        } else {
            updateData(t as CurrencyRates)
        }
    }

    /**
     * Method which renders the currency data into recycler view
     * @param currencyList - CurrencyRates data
     */
    fun renderCurrencyList(currencyList: Map<String, String>) {
        currencyItemViewModelList.clear()
        currencyRatesList.clear()

        var index = 0

        /** Building up the base currency item in the list **/
        val baseCurrencyDetail = MiscUtils.getCurrencyDetail(baseCurrencyCode)
        baseCurrencyDetail?.let {
            marshalCurrencyObject(it, baseCurrencyValue.toString(), index++)
        }

        /** Building up the non base currency item in the list **/
        for ((key, value) in currencyList) {

            val currencyDetail = MiscUtils.getCurrencyDetail(key)
            currencyDetail?.let {
                marshalCurrencyObject(it, value, index++)
            }
        }
    }

    /**
     * Method which construct the currency object
     * @param currencyDetail - Currency Detail object
     * @param curRate - Currency Rate
     * @param index - Currency Item index in the list
     */
    private fun marshalCurrencyObject(currencyDetail: ExtendedCurrency, curRate: String, index: Int) {

        val currencyItemViewModel = CurrencyItemViewModel()
        currencyItemViewModel.apply {
            currencyTitle.value = currencyDetail.code
            flag.value = currencyDetail.flag
            currencyName.value = currencyDetail.name
            currencyRate.value = if (index == 0) curRate else MiscUtils.formatCurrency(curRate.toFloat() * baseCurrencyValue)
            position = index

            /** Setting the OnAmountChangeListener on the first item of the list **/
            if (index == 0)
                listener = object : OnAmountChangeListener {
                    override fun onAmountChange(newAmount: String) {

                        /** Setting the new base currency value **/
                        baseCurrencyValue = if (newAmount != "") MiscUtils.formatCurrencyToFloat(newAmount) else 0f

                        /** Updating all the currency rate list according to base currency value **/
                        for (i in 1 until currencyItemViewModelList.size) {
                            currencyItemViewModelList[i].currencyRate.value = MiscUtils.formatCurrency(currencyRatesList[i] * if (newAmount != "") MiscUtils.formatCurrencyToFloat(newAmount) else 0f)
                        }
                    }
                }
        }

        currencyRatesList.add(curRate.toFloat())
        currencyItemViewModelList.add(currencyItemViewModel)
    }

    /**
     * Method to update the currency rates
     * @param updatedData - updated currency data
     */
    private fun updateData(updatedData: CurrencyRates) {

        for ((key, value) in updatedData.rates) {
            for (currency in currencyItemViewModelList) {
                if (currency.currencyTitle.value == key) {
                    currency.currencyRate.value = MiscUtils.formatCurrency(value.toFloat() * baseCurrencyValue)
                    break
                }
            }
        }
    }

    /**
     * Method to swap the item as base currency
     * @param index - Index of the clicked item
     */
    fun swapCurrencyItems(index: Int) {

        /** Setting up new base currency code on the basis of swapped item **/
        currencyItemViewModelList[index].currencyTitle.value?.let {
            baseCurrencyCode = it
        }
        /** Setting up new base currency value on the basis of swapped item **/
        currencyItemViewModelList[index].currencyRate.value?.let {
            baseCurrencyValue = MiscUtils.formatCurrencyToFloat(it)
        }


        Collections.swap(currencyItemViewModelList, 0, index)
        currencyItemViewModelList[0].position = 0
        currencyItemViewModelList[index].position = index

        isInitialFetch = true

        getCurrencyList()
    }

    /**
     * Method to construct the Currency Service parameters
     * @return - Parameter Map
     */
    private fun getCurrencyParams(baseCurrency: String): MutableMap<String, String> {

        val data = HashMap<String, String>()
        data[Constants.KEY_BASE_CURRENCY_KEY] = baseCurrency
        return data
    }
}