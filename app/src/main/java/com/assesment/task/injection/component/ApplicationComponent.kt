package com.assesment.task.injection.component

import com.assesment.task.dataManager.OperationalDataManager
import com.assesment.task.injection.module.OperationalDataManagerModule
import com.assesment.task.injection.module.RetrofitClientModule
import com.assesment.task.viewModel.CurrencyListFragmentViewModel
import dagger.Component
import javax.inject.Singleton

/**
 * ApplicationComponent for dagger
 * Author: Muhammad Mudasir
 */
@Singleton
@Component(modules = [(RetrofitClientModule::class), (OperationalDataManagerModule::class)])

interface ApplicationComponent {

    fun inject(operationalDataManager: OperationalDataManager)
    fun inject(currencyListFragmentViewModel: CurrencyListFragmentViewModel)
}