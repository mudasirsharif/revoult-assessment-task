package com.assesment.task.injection.module

import com.assesment.task.dataManager.RetrofitClient
import com.assesment.task.utils.Constants
import dagger.Module
import dagger.Provides
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit
import javax.inject.Singleton

/**
 * The Retrofit Module for Dagger which provides dependency object to Operational data Manager
 * Author: Muhammad Mudasir
 */
@Module
class RetrofitClientModule {

    @Singleton
    @Provides
    fun providesRetrofitClient(): RetrofitClient {


        val builder = Retrofit.Builder()
                .baseUrl(Constants.BASE_URL)
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .client(getHttpClient())
                .addConverterFactory(
                        GsonConverterFactory.create()
                )

        val retrofit = builder.build()
        return retrofit.create(RetrofitClient::class.java)
    }

    private fun getHttpClient(): OkHttpClient {

        /** Interceptor to log the service requests **/
        val interceptor: HttpLoggingInterceptor = HttpLoggingInterceptor().apply {
            this.level = HttpLoggingInterceptor.Level.BODY
        }

        return OkHttpClient.Builder().apply {
            this.addInterceptor(interceptor)
            this.callTimeout(Constants.SERVICE_CALL_TIMEOUT, TimeUnit.SECONDS)
        }.build()
    }
}