package com.assesment.task.utils

import com.mynameismidori.currencypicker.ExtendedCurrency

/**
 * The util class which contains the basic utility methods related to string manipulation, validation, etc.
 * Author: Muhammad Mudasir
 */
object MiscUtils {

    /**
     * Method to get the currency detail
     * @param currencyCode - currencyCode
     * @return - currencyDetail
     */
    fun getCurrencyDetail(currencyCode: String): ExtendedCurrency? = ExtendedCurrency.getCurrencyByISO(currencyCode)

    /**
     * Method to format the string up to 3 decimal places with currency format
     * @param currencyValue - currencyValue to format
     * @return - formatted Currency value
     */
    fun formatCurrency(currencyValue: Float): String = String.format("%,.3f", currencyValue)

    /**
     * Method to change the currency format to float for computation
     * @param currencyValue - currencyValue to format to float
     * @return - formatted Currency value
     */
    fun formatCurrencyToFloat(currencyValue: String): Float = currencyValue.replace(",", "").toFloat()

}