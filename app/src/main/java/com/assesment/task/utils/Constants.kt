package com.assesment.task.utils

/**
 * Class which keeps all the constants
 * Author: Muhammad Mudasir
 */
object Constants {

    const val BASE_URL = "https://revolut.duckdns.org/"

    const val KEY_BASE_CURRENCY_KEY = "base"

    const val VALUE_BASE_CURRENCY_KEY = "CAD"

    const val API_CALL_REPETITION = 1L

    const val KEY_SERVICE_ERROR_MESSAGE = "message"

    const val SERVICE_CALL_TIMEOUT = 5L
}