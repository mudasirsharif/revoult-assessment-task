package com.assesment.task.utils

import android.annotation.SuppressLint
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment

@SuppressLint("StaticFieldLeak")

/**
 * Fragment util class which takes care of all types of fragment transitions
 * Author: Muhammad Mudasir
 */
object FragmentUtils {

    /**
     * The method for replacing a new fragment
     * @param fragment: Fragment to be added
     * @param id: Fragment container ID
     * @param addToBackStack: Flag indicating whether to add to back stack or not
     */
    fun replaceFragment(context: AppCompatActivity, fragment: Fragment, id: Int, addToBackStack: Boolean) {

        val fragmentManager = context.supportFragmentManager
        val transaction = fragmentManager.beginTransaction()

        if (addToBackStack)
            transaction.addToBackStack(fragment.javaClass.canonicalName)

        transaction.replace(id, fragment, fragment.javaClass.canonicalName)
        transaction.commit()
    }

}