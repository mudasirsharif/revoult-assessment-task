package com.assesment.task.utils

import androidx.appcompat.app.AppCompatActivity
import com.assesment.task.R
import com.assesment.task.view.fragment.CurrencyListFragment


/**
 * Navigation Util class
 * Author: Muhammad Mudasir
 */
object NavUtils {

    fun pushCurrencyRateListFragment(context: AppCompatActivity) {
        FragmentUtils.replaceFragment(context, CurrencyListFragment(), R.id.fragment_container, false)
    }
}
