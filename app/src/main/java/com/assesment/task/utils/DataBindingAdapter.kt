package com.assesment.task.utils

import androidx.appcompat.widget.AppCompatImageView
import androidx.databinding.BindingAdapter
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions


/**
 * Custom Binding Adapter Class
 * Author: Muhammad Mudasir
 */
object DataBindingAdapter {

    /**
     * Binding adapter to bing country flag image
     * @param imageView - ImageView
     * @param resource - Image Resource
     */
    @BindingAdapter("imageResource")
    @JvmStatic
    fun setImageResource(imageView: AppCompatImageView, resource: Int) {

        Glide.with(imageView)
                .load(resource)
                .skipMemoryCache(true)
                .apply(RequestOptions.circleCropTransform())
                .into(imageView)
    }
}