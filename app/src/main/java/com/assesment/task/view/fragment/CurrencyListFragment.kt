package com.assesment.task.view.fragment


import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.assesment.task.R
import com.assesment.task.base.BaseFragment
import com.assesment.task.databinding.FragmentCurrencyListBinding
import com.assesment.task.model.CurrencyRates
import com.assesment.task.view.DialogFactory
import com.assesment.task.viewModel.CurrencyListFragmentViewModel

/**
 * Currency List Fragment
 * Author: Muhammad Mudasir
 */
class CurrencyListFragment : BaseFragment() {

    private lateinit var currencyListFragmentViewModel: CurrencyListFragmentViewModel
    private lateinit var binding: FragmentCurrencyListBinding

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {

        /** ViewModel binding with view **/
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_currency_list, container, false)

        /** Getting the ViewModel object from ViewModelProvider to retain the object in case of configuration changes **/
        currencyListFragmentViewModel = ViewModelProvider(this).get(CurrencyListFragmentViewModel::class.java)
        binding.viewModel = currencyListFragmentViewModel
        binding.lifecycleOwner = this

        return binding.root

    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        currencyListFragmentViewModel.toolBarTitle.value = getString(R.string.currency_list_fragment_title)

        observerUserClickResponse()
    }

    override fun onResume() {
        super.onResume()

        /** getting the currency list **/
        currencyListFragmentViewModel.getCurrencyList()
    }

    override fun onPause() {
        super.onPause()

        /** Clearing the disposable to stop the service calls for currency rates **/
        currencyListFragmentViewModel.compositeDisposable.clear()
    }

    /**
     * Override method to observe the service response to update the UI
     */
    override fun observerServiceResponse() {
        currencyListFragmentViewModel.serviceResponse.observe(this, Observer {
            if (null != it) {

                /** Storing the data model inside sharedViewModel to share it between multiple fragments **/
                sharedViewModel?.model = it

                currencyListFragmentViewModel.renderCurrencyList((it as CurrencyRates).rates)
            }
        })
    }

    /**
     * Override method to observe the error response
     */
    override fun observerServiceErrorResponse() {
        currencyListFragmentViewModel.serviceErrorResponse.observe(this, Observer {

            activity?.let { context ->
                DialogFactory.showSimpleDialog(context, getString(R.string.error_string), it)
            }
        })
    }

    /**
     * Method to observe the user click on currency list item
     */
    private fun observerUserClickResponse() {
        currencyListFragmentViewModel.userClickResponse.observe(this, Observer {
            if (null != it) {

                currencyListFragmentViewModel.compositeDisposable.clear()
                currencyListFragmentViewModel.swapCurrencyItems(it.toInt())
            }
        })
    }
}