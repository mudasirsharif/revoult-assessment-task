package com.assesment.task.view.activity

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProvider
import com.assesment.task.R
import com.assesment.task.databinding.ActivityContainerBinding
import com.assesment.task.utils.NavUtils
import com.assesment.task.viewModel.ContainerActivityViewModel

/**
 * Activity which will serve as a container for all fragments
 * Author: Muhammad Mudasir
 */
class ContainerActivity : AppCompatActivity() {

    private lateinit var containerActivityViewModel: ContainerActivityViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        /** ViewModel binding with view **/
        val binding: ActivityContainerBinding = DataBindingUtil.setContentView(this, R.layout.activity_container)

        /** Getting the ViewModel object from ViewModelProvider to retain the object in case of configuration changes **/
        containerActivityViewModel = ViewModelProvider(this).get(ContainerActivityViewModel::class.java)
        binding.viewModel = containerActivityViewModel
        binding.lifecycleOwner = this

        /** To avoid pushing the fragment again in case of configuration changes **/
        if (savedInstanceState != null) {
            return
        }

        NavUtils.pushCurrencyRateListFragment(this)
    }
}