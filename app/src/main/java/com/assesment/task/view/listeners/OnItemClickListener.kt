package com.assesment.task.view.listeners

/**
 * Click listener interface for currency item in the list
 * Author: Muhammad Mudasir
 */
interface OnItemClickListener {
    fun onItemClick(item: Any)
}