package com.assesment.task.view.listeners

/**
 * Interface to listen for amount change
 * Author: Muhammad Mudasir
 */
interface OnAmountChangeListener {
    fun onAmountChange(newAmount: String)
}