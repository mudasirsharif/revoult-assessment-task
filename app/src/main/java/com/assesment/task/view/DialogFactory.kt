package com.assesment.task.view

import android.annotation.SuppressLint
import androidx.appcompat.app.AlertDialog
import androidx.fragment.app.FragmentActivity
import com.assesment.task.R

@SuppressLint("StaticFieldLeak")

/**
 * This class can provide various alert dialogs
 * Author: Muhammad Mudasir
 */
object DialogFactory {

    /**
     * Method to show simple dialog
     * @param context - Fragment Activity Context to build the dialog
     * @param title - Title of dialog
     * @param message - Message of dialog
     */
    fun showSimpleDialog(context: FragmentActivity, title: String, message: String?) {

        val builder = AlertDialog.Builder(context)
        builder.setTitle(title)
        builder.setMessage(message)
        builder.setPositiveButton(context.getString(R.string.dialog_cancel_button_text)) { dialog, _ ->
            dialog.dismiss()
        }
        val dialog: AlertDialog = builder.create()
        dialog.show()
    }

}