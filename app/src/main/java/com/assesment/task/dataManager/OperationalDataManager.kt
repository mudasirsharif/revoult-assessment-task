package com.assesment.task.dataManager

import com.assesment.task.RevoltTaskApplication
import com.assesment.task.model.CurrencyRates
import io.reactivex.Observable
import javax.inject.Inject

/**
 * OperationalDataManager which is responsible for performing the service calls from one point
 * Author: Muhammad Mudasir
 */
class OperationalDataManager : DataManager() {

    @Inject
    lateinit var retrofitClient: RetrofitClient

    /**
     * Initializer block to inject the retrofitClient object
     */
    init {
        RevoltTaskApplication.getAppComponent().inject(this)
    }

    /**
     * Currency Rate List Service Request
     * @param params - Currency Rate List Service Call Parameters
     */
    fun currencyRateServiceRequest(params: Map<String, String>): Observable<CurrencyRates> =

            repetitiveServiceRequest(retrofitClient.getCurrencyRates(params))
}