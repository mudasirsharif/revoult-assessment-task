package com.assesment.task.dataManager

import com.assesment.task.utils.Constants
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import java.util.concurrent.TimeUnit

/**
 * The class which is responsible to perform all the service requests.
 * Author: Muhammad Mudasir
 */
abstract class DataManager {

    fun <T> repetitiveServiceRequest(retrofitObservableClient: Observable<T>): Observable<T> =

            Observable.interval(0, Constants.API_CALL_REPETITION, TimeUnit.SECONDS)
                    .flatMap { retrofitObservableClient }
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
}