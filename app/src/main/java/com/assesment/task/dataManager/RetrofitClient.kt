package com.assesment.task.dataManager

import com.assesment.task.model.CurrencyRates
import io.reactivex.Observable
import retrofit2.http.GET
import retrofit2.http.QueryMap

/**
 * API Interface for all the service calls
 * Author: Muhammad Mudasir
 */
interface RetrofitClient {

    @GET("latest")
    fun getCurrencyRates(@QueryMap options: Map<String, String>): Observable<CurrencyRates>
}