package com.assesment.task.base

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.assesment.task.R
import com.assesment.task.RevoltTaskApplication
import com.assesment.task.utils.Constants
import io.reactivex.Observer
import io.reactivex.disposables.Disposable
import okhttp3.ResponseBody
import org.json.JSONObject
import retrofit2.HttpException
import java.io.IOException
import java.net.SocketTimeoutException


/**
 * This class serves as the Base ViewModel class. All other ViewModel classes should be extended from this
 * Author: Muhammad Mudasir
 */
abstract class BaseViewModel : ViewModel(), Observer<BaseModel> {

    var loading: MutableLiveData<Boolean> = MutableLiveData(true)
    val serviceResponse: MutableLiveData<BaseModel> = MutableLiveData()
    val serviceErrorResponse: MutableLiveData<String> = MutableLiveData()

    val toolBarTitle: MutableLiveData<String> = MutableLiveData()

    var disposable: Disposable? = null

    /** Overriding the observer callback methods **/

    override fun onComplete() {}

    override fun onSubscribe(d: Disposable) {
        disposable = d
    }

    override fun onNext(t: BaseModel) {
        loading.value = false
    }

    /** Handling the service errors for every request **/
    override fun onError(e: Throwable) {
        loading.value = false

        when (e) {
            is HttpException -> {
                val responseBody = e.response().errorBody()
                serviceErrorResponse.value = getErrorMessage(responseBody)
            }
            is SocketTimeoutException -> serviceErrorResponse.value = e.message
            is IOException -> serviceErrorResponse.value = e.message
            else -> serviceErrorResponse.value = RevoltTaskApplication.applicationContext().getString(R.string.general_error_message)
        }
    }

    /**
     * @param responseBody - Network error response
     * @return - error message
     */
    private fun getErrorMessage(responseBody: ResponseBody?): String? {
        return try {
            val jsonObject = JSONObject(responseBody?.string())
            jsonObject.getString(Constants.KEY_SERVICE_ERROR_MESSAGE)
        } catch (e: Exception) {
            e.message
        }
    }
}