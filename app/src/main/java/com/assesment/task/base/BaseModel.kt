package com.assesment.task.base

/**
 * This class serves as the Base Model class. All other model classes should be extended from this
 * Author: Muhammad Mudasir
 */
open class BaseModel