# Revoult Assessment Task

This app is for the purpose of Task assessmennt. To Run the app, please follow the following steps.

Open Android studio and go to VCS -> Git -> Clone
Put the repository url, define the destination folder and hit Clone button
After cloning, open the app in Android Studio and Run the app
Gradle Version: 3.4.2 minSdkVersion: 18 targetSdkVersion: 28

App follows the MVVM architecture
App Contains, one container activity and one fragment.
CurrencyListFragment is being used to show the currency rate list.
OOP concepts is being used to minimize the code reuse, specially when app size increases

Libraries used:

Dagger 2 for dependency injection https://github.com/google/dagger
Retrofit as REST api https://github.com/square/retrofit
Binding Collection View Adapters for recycler view https://github.com/evant/binding-collection-adapter
Lottie animation by AirBnb https://github.com/airbnb/lottie-android
Android Architecture components (Live Data, ViewModel Providers)
Data Binding
RxJava
Glide

